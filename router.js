const express = require('express')
const controller = require('./controller.js')
const router = express.Router();

const usersPath = '/users'
const userPath = `${usersPath}/:id`

router.get(usersPath, controller.getUsers);
router.get(userPath, controller.getUser);
router.put(userPath, controller.putUser);
router.post(usersPath, controller.postUser);
router.delete(userPath, controller.deleteUser);

router.get(`${userPath}/posts`, controller.getUserPosts);
router.get(`${userPath}/albums`, controller.getUserAlbums);

router.get(`${usersPath}/albums/photos`, controller.getUserAlbumsPhotos);



module.exports = router
