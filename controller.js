const service = require('./axiosService.js')


const getUsers = async (req, res, next) => {
    try {
        const users = await service.getUsers();
        res.json(users)
    } catch (err) {
        next(err);
    }
}

const getUser = async (req, res, next) => {
    try {
        const user = await service.getUser(Number(req.params.id));
        res.json(user)
    } catch(err) {
        next(err)
    }
}

const putUser = async (req, res, next) => {
    try {
        const user = await service.putUser(Number(req.params.id), req.body);
        res.json(user)
    } catch(err) {
        next(err)
    }
}

const postUser = async (req, res, next) => {
    try {
        const user = await service.postUser(req.body);
        res.json(user)

    } catch(err) {
        next(err)
    }
}

const deleteUser = async (req, res, next) => {
    try {
        const user = await service.deleteUser(req.params.id);
        res.json(user)
    } catch(err) {
        next(err)
    }
}

const getUserPosts = async (req, res, next) => {
    try {
        const userPosts = await service.getUserPosts(req.params.id);
        res.json(userPosts)
    } catch(err) {
        next(err)
    }
}

const getUserAlbums = async (req, res, next) => {
    try {
        const userAlbums = await service.getUserAlbums(req.params.id);
        res.json(userAlbums)
    } catch(err) {
        next(err)
    }
}

const getUserAlbumsPhotos = async (req, res, next) => {
    try {
        const users = await service.getUsers()
        const albums = await service.getAlbums()
        const photos = await service.getPhotos()
        for (let user of users) {
            user.relationships = { albums: { data: [] } }
            for (let album of albums) {
                album.relationships = { photos: { data: [] } }
                if (user.id === album.userId) {
                    user.relationships.albums.data.push(album)
                }
                for (let photo of photos) {
                    if (album.id === photo.albumId) {
                        album.relationships.photos.data.push(photo)
                    }
                }
            }
        }
        res.json(users)
    } catch(err) {
        next(err)
    }
}


module.exports = {
    getUsers,
    getUser,
    putUser,
    postUser,
    deleteUser,
    getUserPosts,
    getUserAlbums,
    getUserAlbumsPhotos
}