const axios = require('axios').default;


const getUsers = async () => {
    const { data } = await axios.get('https://jsonplaceholder.typicode.com/users')
    return data;
}

const getUser = async (id) => {
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
    return data;
}

const putUser = async (id, body) => {
    const { data } = await axios.put(`https://jsonplaceholder.typicode.com/users/${id}`, body)
    return data;
}

const postUser = async (body) => {
    const { data } = await axios.post('https://jsonplaceholder.typicode.com/users', body)
    return data;
}

const deleteUser = async (id) => {
    const { data } = await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
    return data;
}

const getUserPosts = async (id) => {
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}/posts`)
    return data;
}

const getUserAlbums = async (id) => {
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}/albums`)
    return data;
}

const getAlbums = async () => {
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/albums`)
    return data
}

const getPhotos = async () => {
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/photos`)
    return data
}

module.exports = {
    getUsers,
    getUser,
    putUser,
    postUser,
    deleteUser,
    getUserPosts,
    getUserAlbums,
    getAlbums,
    getPhotos
}